import express from "express";
import data from "./dataProduct.js";

const app = express();

app.get("/api/products", (request, response) => {
  response.send(data.products);
});

const port = process.env.PORT || 5000;
app.listen(port, () => {
  console.log(`Server is running great at port number ${port}`);
});
